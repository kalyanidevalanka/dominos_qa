var assert = require('assert');
let input=require('./elements');
let inputD=require('./testdata');
const expect = require('chai').expect;
const {Builder, By, until,sendKeys,timeout,isExisting} = require('selenium-webdriver');


module.exports = function () {
    this.Given(/^I visit dominos$/, function() {
        return this.driver.get(input.url);
        console.log("checking");

    });

        this.Then(/^I should see dominos in title$/, function () {

            this.driver.getTitle().then(function (title) {
                //assert.equal(title, "Domino\\\\\\'s Pizza: Order Pizza Online | Get 2 Regular Pizzas @ 99 Each | Everyday Value Offers");
             try {
                 throw  expect(title).to.include('Dominos', 'URL Mismatch');

                 return title;
             }
                catch (e) {
                     console.log("exception is "+e);
                }
           });


        });
    this.Given(/^click on english$/, function() {
        return this.driver.wait(until.elementLocated(By.linkText(input.englishbutton)), 30000, 'Looking for element').click();

    });
    this.Given(/^Click on Delivery$/, function() {
        return this.driver.wait(until.elementLocated(By.linkText(input.deliverybutton)), 30000, 'Looking for element').click();

    },30000);

    this.Given(/^Fill the Adress$/, async function() {
        await new Promise(resolve => setTimeout(resolve, 6000));
     return this.driver.wait(until.elementLocated(By.id(input.StreetName)), 80000, 'Looking for element').sendKeys(inputD.Streetname)
    .then(()=> this.driver.wait(until.elementLocated(By.id(input.StreetNumber)), 80000, 'Looking for element').sendKeys(inputD.Doornumber))
    .then(()=>this.driver.wait(until.elementLocated(By.id(input.Postal_Code)), 80000, 'Looking for element').sendKeys(inputD.PostalCode))
  .then(()=>this.driver.wait(until.elementLocated(By.id(input.City)), 50000, 'Looking for element').click());
    });


    this.Then(/^I should select city$/, function() {
        return  this.driver.wait(
            until.elementLocated(By.id(input.City)), 20000
        ).then(element => {
            selectByVisibleText(element, inputD.City)
        });

        function selectByVisibleText(select, textDesired) {
            select.findElements(By.tagName(input.select))
                .then(options => {
                    options.map(option => {
                       // console.log(option.getText());
                        option.getText().then(text => {
                            if (text == textDesired)
                                option.click();
                        });
                    });
                });
        };
    });
    this.Then(/^click on continue$/,async function() {
        await new Promise(resolve => setTimeout(resolve, 8000));
        return this.driver.wait(until.elementLocated(By.className(input.Continue)), 50000, 'Looking for element').click();
    });
    this.Then(/^click on ourmenupizza$/,async function() {
        await new Promise(resolve => setTimeout(resolve, 8000));
        return this.driver.wait(until.elementLocated(By.xpath(input.ourmenupizza)), 50000, 'Looking for element').click();
    });
    this.Then(/^click on Addorder$/,async function() {

        await new Promise(resolve => setTimeout(resolve, 8000));

      return this.driver.wait(until.elementLocated(By.xpath(input.Addorder)), 50000, 'Looking for element').click()
          .then(()=>assert.equal(inputD.checkout,"something"));

    });
    this.Then(/^click on checkout$/,async function() {
        await new Promise(resolve => setTimeout(resolve, 8000));
        return this.driver.wait(until.elementLocated(By.xpath(input.checkout)), 50000, 'Looking for element').click();
    });
    this.Then(/^click on nogotocheckout$/,async function() {
        await new Promise(resolve => setTimeout(resolve, 8000));
        return this.driver.wait(until.elementLocated(By.xpath(input.nogotocheckout)), 50000, 'Looking for element').click();
    });
    this.Then(/^click on waterbottle$/,async function() {
        await new Promise(resolve => setTimeout(resolve, 8000));
        return this.driver.wait(until.elementLocated(By.xpath(input.waterbottle)), 50000, 'Looking for element').click();
    });
    this.Then(/^click on add to order$/,async function() {
        await new Promise(resolve => setTimeout(resolve, 8000));
        return this.driver.wait(until.elementLocated(By.xpath(input.Addtoorder)), 50000, 'Looking for element').click();
    });
    this.Then(/^click on Continuetocheckout$/,async function() {
        await new Promise(resolve => setTimeout(resolve, 8000));
        return this.driver.wait(until.elementLocated(By.xpath(input.Continuetocheckout)), 50000, 'Looking for element').click();
    });
    this.Given(/^fill the Delivery_Instructions$/, async function() {
        await new Promise(resolve => setTimeout(resolve, 5000));
        return this.driver.wait(until.elementLocated(By.id(input.Delivery_Instructions)), 80000, 'Looking for element').sendKeys(inputD.Delivery_Instructions)

    });
    this.Given(/^fill the userdetails$/, async function() {
        await new Promise(resolve => setTimeout(resolve, 5000));
        return this.driver.wait(until.elementLocated(By.id(input.First_Name)), 80000, 'Looking for element').sendKeys(inputD.firstname)
            .then(()=>this.driver.wait(until.elementLocated(By.id(input.Last_Name)), 80000, 'Looking for element').sendKeys(inputD.lastname))
            .then(()=>this.driver.wait(until.elementLocated(By.id(input.Email)), 80000, 'Looking for element').sendKeys(inputD.mailid))
            .then(()=>  this.driver.wait(until.elementLocated(By.id(input.Callback_Phone)), 80000, 'Looking for element').sendKeys(inputD.mobileno));

    });

    this.Then(/^click on the checkbox$/,async function() {
        await new Promise(resolve => setTimeout(resolve, 8000));
        return this.driver.wait(until.elementLocated(By.id(input.checkbox)), 50000, 'Looking for element').click();
    });
    this.Then(/^click on accept checkbox$/,async function() {
        await new Promise(resolve => setTimeout(resolve, 8000));
        return this.driver.wait(until.elementLocated(By.id(input.Agree_To_Terms_Of_Use)), 50000, 'Looking for element').click();
    });
    this.Given(/^fill the custmortaxnumber$/, async function() {
        await new Promise(resolve => setTimeout(resolve, 5000));
        return this.driver.wait(until.elementLocated(By.name(input.Tax_ID)), 80000, 'Looking for element').sendKeys(inputD.Tax_ID);

    });
    this.Then(/^click on sameaddress checkbox$/,async function() {
        await new Promise(resolve => setTimeout(resolve, 8000));
        return this.driver.wait(until.elementLocated(By.id(input.Use_Delivery_Address)), 50000, 'Looking for element').click();
    });
    this.Given(/^fill the custmorname$/, async function() {
        await new Promise(resolve => setTimeout(resolve, 5000));
        return this.driver.wait(until.elementLocated(By.name(input.Tax_ID_CustomerName)), 80000, 'Looking for element').sendKeys(inputD.Tax_ID_CustomerName);

    });
};
